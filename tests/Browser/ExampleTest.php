<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function (Browser $browser) {
            //HomePage
            $browser->visit('http://demowebshop.tricentis.com/')
                    ->assertSee('Tricentis Demo Web Shop')
                    ->assertSee('Register')
                    ->assertSee('Log in')
                    ->assertSee('Sign up for our newsletter:')
                    //->assertSee('Forgot password?')
                    ->assertSee('Shopping cart')
                    ->assertSee('Welcome to our store')
                    ->assertSee('Welcome to the new Tricentis store!')
                    ->assertSee('Feel free to shop around and explore everything.')
                    ->pause(1000);
                });
            }
    public function testLoginPageWrongEmail_Fail(){
                //login fail
    $this->browse(function (Browser $browser) {
            $browser->visit('http://demowebshop.tricentis.com/login')
                ->assertSee('Demo Web Shop')
                ->type('Email', 'comcomcom')
                ->type('Password', 'tester')
                ->check('RememberMe')
                ->press('Log in')
                ->assertSee('Please enter a valid email address.');
            });
        }        

    public function testLoginPageWrongPassword_Fail(){
                    //login fail
        $this->browse(function (Browser $browser) {
                $browser->visit('http://demowebshop.tricentis.com/login')
                    ->assertSee('Demo Web Shop')
                    ->type('Email', 'zare_ns@yahoo.com')
                    ->type('Password', 'wrong')
                    ->check('RememberMe')
                    ->press('Log in')
                    ->assertSee('Login was unsuccessful. Please correct the errors and try again.');
                });
            }

    public function testLoginPage_Success(){    
                    //login pass
        $this->browse(function (Browser $browser) {
                $browser->visit('http://demowebshop.tricentis.com/login')  
                    ->assertSee('Demo Web Shop')
                    ->type('Email', 'zare_ns@yahoo.com')
                    ->type('Password', 'tester')
                    ->check('RememberMe')
                    ->press('Log in')
                    ->assertSee('zare_ns@yahoo.com')
                    ->assertSee('Log out')
                    ->click('div.master-wrapper-page:nth-child(4) div.master-wrapper-content div.header:nth-child(2) div.header-links-wrapper div.header-links ul:nth-child(1) li:nth-child(2) > a.ico-logout')
                    ->pause(2000);
                });
            }

    public function testRegistrationPage_Required(){    
                //registration required fields
        $this->browse(function (Browser $browser) {
            $browser->visit('http://demowebshop.tricentis.com/register')  
                ->assertSee('Register')
                ->assertSee('Your Personal Details')
                ->assertSee('Gender:')
                ->assertRadioNotSelected('Gender', 'M')
                ->assertRadioNotSelected('Gender', 'F')
                ->assertSee('First name:')
                ->assertSee('Last name:')
                ->assertSee('Email:')
                ->assertSee('Your Password')
                ->assertSee('Password:')
                ->assertSee('Confirm password:')
                ->press('Register')
                ->assertSee('First name is required.')
                ->assertSee('Last name is required.')
                ->assertSee('Email is required.')
                ->assertSee('Password is required.')
                ->assertSee('Password is required.')
                ->type('FirstName', 'zare')
                ->type('LastName', 'zare')
                ->type('Email', 'zare')
                //->pause(2000)
                ->assertSee('Wrong email')
                ->type('Password', '123')
                ->type('ConfirmPassword', '12')
                ->assertSee('The password should have at least 6 characters.')
                ->assertSee(' The password and confirmation password do not match.')
                ->pause(2000);
            });
        }        

    public function testRegistrationPage_EgxistingUser(){    
            //registration fail egxiisting user 
    $this->browse(function (Browser $browser) {
            $browser->visit('http://demowebshop.tricentis.com/register')  
                ->assertSee('Register')
                ->assertSee('Your Personal Details')
                ->assertSee('Gender:')
                ->assertRadioNotSelected('Gender', 'M')
                ->assertRadioNotSelected('Gender', 'F')
                ->radio('Gender', 'M')
                ->assertSee('First name:')            
                ->assertSee('Last name:')
                ->assertSee('Email:')
                ->assertSee('Your Password')
                ->assertSee('Password:')
                ->assertSee('Confirm password:')
                ->type('FirstName', 'zare')
                ->type('LastName', 'zare')
                ->type('Email', 'zare@mailinator.com')
                //->pause(2000)
                ->type('Password', 'tester')
                ->type('ConfirmPassword', 'tester')
                //->pause(2000)
                ->press('Register')
                //->pause(8000)
                ->assertSee('The specified email already exists');
            });
        }        
    public function testRegistrationPage_Success(){    
        //registration sucess
    $this->browse(function (Browser $browser) {
            $browser->visit('http://demowebshop.tricentis.com/register')  
                ->assertSee('Register')
                ->assertSee('Your Personal Details')
                ->assertSee('Gender:')
                ->assertRadioNotSelected('Gender', 'M')
                ->assertRadioNotSelected('Gender', 'F')
                ->radio('Gender', 'M')
                ->assertSee('First name:')            
                ->assertSee('Last name:')
                ->assertSee('Email:')
                ->assertSee('Your Password')
                ->assertSee('Password:')
                ->assertSee('Confirm password:')
                ->type('FirstName', 'zare')
                ->type('LastName', 'zare')
                ->type('Email', 'zare9@mailinator.com')
                //->pause(2000)
                ->type('Password', 'tester')
                ->type('ConfirmPassword', 'tester')
                //->pause(2000)
                ->press('Register')
                ->assertSee('Register')
                ->assertSee('Your registration completed')
                ->press('Continue')
                ->assertSee('zare9@mailinator.com');
            });
        }
}